//
//  ascii_art_string.cpp
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/15/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#include "ascii_art_string.h"
#include "ascii_art_character.h"

std::string AsciiArtString::to_string() {
    return _string;
}

AsciiArtString * AsciiArtString::ParseStringArray(std::string strings[3]) {
    AsciiArtString *retVal;
    std::string s = std::string();
    std::vector<AsciiArtCharacter*> characters;
    bool hasUnrecognizedCharacter = false;
    
    for (int i = 0; i < strings[0].length() / 3; i++) {
        int pos = i * 3;
        AsciiArtCharacter *character = CharacterFromStringArray(&strings[0], pos);
        if (!character->IsRecognized()) {
            hasUnrecognizedCharacter = true;
        }
        characters.push_back(std::move(character));
        s += character->character();
    }
    
    retVal = new AsciiArtString(s, characters);
    retVal->_hasUnrecognizedCharacter = hasUnrecognizedCharacter;
    return retVal;
}

AsciiArtCharacter * AsciiArtString::CharacterFromStringArray(const std::string *strings, int offset) {
    char characterData[3][3];
    for (int j = 0; j < 3; j++) {
        characterData[0][j] = strings[0][offset + j];
        characterData[1][j] = strings[1][offset + j];
        characterData[2][j] = strings[2][offset + j];
    }
    return AsciiArtCharacter::ParseCharacterArray(&characterData[0][0]);
}

AsciiArtString::~AsciiArtString() {
    _characters.clear();
}

std::vector<std::string> AsciiArtString::PossibleSubstitutions() {
    std::vector<std::string> retVal;

    for (int i = 0; i < _characters.size(); i++) {
        AsciiArtCharacter *character = _characters[i];
        std::vector<char> possibleSubstitutions = character->PossibleSubstitutions();
        for (int j = 0; j < possibleSubstitutions.size(); j++) {
            std::string sub = _string;
            sub[i] = possibleSubstitutions[j];
            if (sub.find_first_of('?') == std::string::npos) {
                retVal.push_back(sub);
            }
        }
    }
    
    return retVal;
}

AsciiArtString::AsciiArtString(std::string string, std::vector<AsciiArtCharacter*> characters) {
    _string = string;
    _characters = characters;
}

bool AsciiArtString::HasUnrecognizedCharacter() {
    return _hasUnrecognizedCharacter;
}