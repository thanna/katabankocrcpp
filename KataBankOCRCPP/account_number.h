//
//  account_number.h
//  KataBankOCRCPP
//
//  This class represents an account number.  It is capable of validating itself via a checksum
//
//  Created by Travis Hanna on 3/17/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#ifndef __KataBankOCRCPP__account_number__
#define __KataBankOCRCPP__account_number__

#include <iostream>
#include <vector>

class AccountNumber {
  public:
    AccountNumber(const std::string number);
    bool IsValid();
    std::string to_string();
  
  private:
    int CalculateChecksum();
    std::vector<short> ConvertToDigits();
    void ReverseDigits(std::vector<short> &digits);
    std::string _number;
};

#endif /* defined(__KataBankOCRCPP__account_number__) */
