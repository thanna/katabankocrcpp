//
//  ascii_art_character.cpp
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/14/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#include "ascii_art_character.h"
#include "string_utils.h"

char AsciiArtCharacter::character() {
    return _character;
}

bool AsciiArtCharacter::IsRecognized() {
    return _is_recognized;
}

AsciiArtCharacter * AsciiArtCharacter::ParseCharacterArray(const char *arr) {
    AsciiArtCharacter *retVal;
    std::string s = std::string(arr, 9);
    string_to_string_map::const_iterator found = _sequenceMap.find(s);
    
    if (found != _sequenceMap.end()) {
        std::string matchedChar = found->second;
        retVal = new AsciiArtCharacter(matchedChar[0], s, true);
    }
    else {
        retVal = new AsciiArtCharacter('?', s, false);
    }
    return retVal;
}

std::vector<char> AsciiArtCharacter::PossibleSubstitutions() {
    std::vector<char> retVal;
    
    string_to_char_vector_map::const_iterator found = _substitutionMap.find(_data);
    if (found != _substitutionMap.end()) {
        retVal = found->second;
    }
    
    return retVal;
}

AsciiArtCharacter::AsciiArtCharacter(const char character, const std::string data, const bool is_recognized) {
    _character = character;
    _is_recognized = is_recognized;
    _data = data;
}

string_to_string_map AsciiArtCharacter::_sequenceMap = {
    {std::string(" _ | ||_|"), std::string("0")},
    {std::string("     |  |"), std::string("1")},
    {std::string(" _  _||_ "), std::string("2")},
    {std::string(" _  _| _|"), std::string("3")},
    {std::string("   |_|  |"), std::string("4")},
    {std::string(" _ |_  _|"), std::string("5")},
    {std::string(" _ |_ |_|"), std::string("6")},
    {std::string(" _   |  |"), std::string("7")},
    {std::string(" _ |_||_|"), std::string("8")},
    {std::string(" _ |_| _|"), std::string("9")}
};

string_to_char_vector_map AsciiArtCharacter::InitializeSubstitutionMap() {
    string_to_char_vector_map retVal;
    for (string_to_string_map::iterator it = _sequenceMap.begin(); it != _sequenceMap.end(); ++it) {
        std::string key = it->first;
        std::string value = it->second;
        AddSubstitutionsForSequence(key, value[0], retVal);
    }
    return retVal;
}

void AsciiArtCharacter::AddSubstitutionsForSequence (const std::string &sequence, const char character, string_to_char_vector_map &map) {
    for (int i = 0; i < sequence.length(); i++) {
        std::string substitution = sequence;
        if (IsUnderscorePosition(i)) {
            ToggleBetweenCharacterAndSpace(i, '_', substitution);
            AddSubstitution(substitution, character, map);
        }
        else if (IsPipePosition(i)) {
            ToggleBetweenCharacterAndSpace(i, '|', substitution);
            AddSubstitution(substitution, character, map);
        }
    }
}

void AsciiArtCharacter::AddSubstitution(const std::string &key, const char value, string_to_char_vector_map &map) {
    string_to_char_vector_map::const_iterator found = map.find(key);
    
    std::vector<char> matchedChars;
    
    if (found != map.end()) {
        matchedChars = found->second;
    }

    matchedChars.push_back(value);
    map[key] = matchedChars;
}

bool AsciiArtCharacter::IsPipePosition(const int i) {
    return i == 3 || i == 5 || i == 6 || i == 8;
}

bool AsciiArtCharacter::IsUnderscorePosition(const int i) {
    return i == 1 || i == 4 || i == 7;
}

string_to_char_vector_map AsciiArtCharacter::_substitutionMap = InitializeSubstitutionMap();
