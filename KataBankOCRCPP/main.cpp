//
//  main.cpp
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/13/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#include <iostream>
#include <fstream>
#include "ocr_application.h"

int main(int argc, const char * argv[])
{
    OCRApplication app(argv[1]);
    return app.run();
}

