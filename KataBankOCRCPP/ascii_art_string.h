//
//  ascii_art_string.h
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/15/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//
//  This class parses multi-line ascii art text made up of 3x3 characters into strings. Example:
//      _  _     _  _  _  _  _
//    | _| _||_||_ |_   ||_||_|
//    ||_  _|  | _||_|  ||_| _|
//
//  This class is immutable.  Instances are created via the parse method(s).  Once created, the
//  instance can tell you what string was parsed.
//

#ifndef __KataBankOCRCPP__ascii_art_string__
#define __KataBankOCRCPP__ascii_art_string__

#include <iostream>
#include <vector>
#include "ascii_art_character.h"

class AsciiArtString {
  public:
    std::string to_string(); // The string which was parsed
    bool HasUnrecognizedCharacter(); // True if any character could not be read
    std::vector<std::string> PossibleSubstitutions();
    static AsciiArtString * ParseStringArray(std::string strings[]);
    ~AsciiArtString();
    
  private:
    AsciiArtString(std::string string, std::vector<AsciiArtCharacter*> characters);
    static AsciiArtCharacter * CharacterFromStringArray(const std::string *strings, int offset);
    std::vector<AsciiArtCharacter*> _characters;
    std::string _string;
    bool _hasUnrecognizedCharacter;
};

#endif /* defined(__KataBankOCRCPP__ascii_art_string__) */
