//
//  ocr_app.cpp
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/17/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.

#include "ocr_application.h"
#include "ascii_art_string.h"
#include "account_number.h"
#include <fstream>

OCRApplication::OCRApplication(const char* filename) {
    _filename = filename;
}

int OCRApplication::run() {
    std::ifstream infile;
    std::string record[3];
    
    infile.open(_filename);    
    int i = 0;
    while (!infile.eof()) {
        if (i % 4 != 3) {
            getline(infile, record[i % 4]);
        }
        else {
            processRecord(record);
            getline(infile, record[0]);
        }
        i++;
    }
    
    return 0;
}

void OCRApplication::processRecord(std::string record[3]) {
    std::unique_ptr<AsciiArtString> parsedString(AsciiArtString::ParseStringArray(record));
    std::vector<std::string> possibleAccountNumbers = PossibleAccountNumbers(*parsedString);
    if (possibleAccountNumbers.size() > 0) {
        AccountNumber accountNumber(parsedString->to_string());
        if (accountNumber.IsValid() || possibleAccountNumbers.size() > 1) {
            possibleAccountNumbers.insert(possibleAccountNumbers.begin(), parsedString->to_string());
        }
        outputAmbiguousRecord(possibleAccountNumbers);
    }
    else
    {
        outputNonAmbiguousRecord(parsedString.get());
    }    
}

std::vector<std::string> OCRApplication::PossibleAccountNumbers(AsciiArtString str)
{
    std::vector<std::string> retVal;
    
    std::vector<std::string> substitutions = str.PossibleSubstitutions();
    for (int i = 0; i < substitutions.size(); i++) {
        AccountNumber accountNumber(substitutions[i]);
        if (accountNumber.IsValid()) {
            retVal.push_back(accountNumber.to_string());
        }
    }
    
    return retVal;
}

void OCRApplication::outputNonAmbiguousRecord(AsciiArtString *number) {
    if (number->HasUnrecognizedCharacter()) {
        outputInvalidAccountNumber(number);
    }
    else {
        AccountNumber accountNumber(number->to_string());
        if (accountNumber.IsValid()) {
            outputValidAccountNumber(number);
        }
        else {
            outputReadError(number);
        }
    }
}

void OCRApplication::outputAmbiguousRecord(std::vector<std::string> numbers) {
    if (numbers.size() == 1) {
        std::cout << numbers[0];
    }
    else {
        std::cout << numbers[0] << " AMB [";
        for (int i = 1; i < numbers.size(); i++) {
            std::cout << "'" << numbers[i] << "'";
            if (i + 1 == numbers.size()) {
                std::cout << ']';
            }
            else {
                std::cout << ", ";
            }
        }
    }
    std::cout << "\n";        
}

void OCRApplication::outputReadError(AsciiArtString *number) {
    std::cout << number->to_string() << " ERR" << "\n";
}

void OCRApplication::outputInvalidAccountNumber(AsciiArtString *number) {
    std::cout << number->to_string() << " ILL" << "\n";
}

void OCRApplication::outputValidAccountNumber(AsciiArtString *number) {
    std::cout << number->to_string() << "\n";
}