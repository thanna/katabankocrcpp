//
//  ascii_art_character.h
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/14/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//
//  This class represents an ascii art character composed via a 3x3 grid
//  For example, the character 3 would be represented as:
//     _
//     _|
//     _|
//
//  This class is immutable.  Instances are created via the parsing method(s).  Once created
//  the class can tell you what character was parsed (if it recognized one).
//

#ifndef __KataBankOCRCPP__ascii_art_character__
#define __KataBankOCRCPP__ascii_art_character__

#include <iostream>
#include <unordered_map>
#include <vector>

typedef std::unordered_map<std::string, std::string> string_to_string_map;
typedef std::unordered_map< std::string, std::vector<char> > string_to_char_vector_map;

class AsciiArtCharacter {
  public:
    char character(); // The character represented by the parsed data
    bool IsRecognized(); // True if a valid character was found
    static AsciiArtCharacter * ParseCharacterArray(const char *); // Expects a 3x3 character array
    std::vector<char> PossibleSubstitutions(); // Other characters that are one pipe or underscore removed
    
  private:
    AsciiArtCharacter(const char character, const std::string data, const bool is_recognized);

    static string_to_char_vector_map InitializeSubstitutionMap();
    static void AddSubstitutionsForSequence (const std::string &sequence, const char character, string_to_char_vector_map &map);
    static void AddSubstitution(const std::string &key, const char value, string_to_char_vector_map &map);
    static bool IsUnderscorePosition(const int i);
    static bool IsPipePosition(const int i);

    char _character;
    bool _is_recognized;
    std::string _data;

    static string_to_string_map _sequenceMap;
    static string_to_char_vector_map _substitutionMap;    
};

#endif /* defined(__KataBankOCRCPP__ascii_art_character__) */
