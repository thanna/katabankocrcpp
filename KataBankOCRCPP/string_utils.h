//
//  string_utils.h
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/14/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#ifndef __KataBankOCRCPP__char_utils__
#define __KataBankOCRCPP__char_utils__

#include <iostream>

// Toggles the character at the given position in the array back and forth between the specified
// character and a space
void ToggleBetweenCharacterAndSpace (int position, char toggleWith, std::string &str);

#endif /* defined(__KataBankOCRCPP__char_utils__) */
