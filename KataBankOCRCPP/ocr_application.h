//
//  ocr_app.h
//  KataBankOCRCPP
//
//  This is the main application class for the ORC application.  It maintains any necessary
//  state during execution and executes the application's primary tasks.
//
//  Created by Travis Hanna on 3/17/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#ifndef __KataBankOCRCPP__ocr_app__
#define __KataBankOCRCPP__ocr_app__

#include <iostream>
#include <vector>
#include "ascii_art_string.h"

class OCRApplication {
  public:
    OCRApplication(const char* filename);
    int run();

  private:
    const char* _filename;
    void processRecord(std::string record[3]);
    std::vector<std::string> PossibleAccountNumbers(AsciiArtString str);
    void outputNonAmbiguousRecord(AsciiArtString *number);
    void outputAmbiguousRecord(std::vector<std::string> numbers);
    void outputReadError(AsciiArtString *number);
    void outputInvalidAccountNumber(AsciiArtString *number);
    void outputValidAccountNumber(AsciiArtString *number);
};
#endif /* defined(__KataBankOCRCPP__ocr_app__) */
