//
//  account_number.cpp
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/17/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#include "account_number.h"

AccountNumber::AccountNumber(const std::string number) {
    _number = number;
}

bool AccountNumber::IsValid() {
    if (CalculateChecksum() % 11 == 0) {
        return true;
    }
    return false;
}

std::string AccountNumber::to_string() {
    return _number;
}

int AccountNumber::CalculateChecksum() {
    std::vector<short> digits = ConvertToDigits();
    ReverseDigits(digits);
    int total = 0;
    for (int i = 0; i < digits.size(); i++) {
        total += (i+1) * digits[i];
    }
    return total;
}

std::vector<short>  AccountNumber::ConvertToDigits() {
    std::vector<short>retVal(_number.size());
    for (int i = 0; i < _number.size(); i++) {
        retVal[i] = _number[i] - '0';
    }
    return retVal;
}


void AccountNumber::ReverseDigits(std::vector<short> &digits) {
    short temp;
    for (int i = 0; i < digits.size() / 2; i++)
    {
        temp = digits[i];
        digits[i] = digits[digits.size() - i - 1];
        digits[digits.size() - i - 1] = temp;
    }
}

