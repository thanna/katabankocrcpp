//
//  string_utils.cpp
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/14/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#include "string_utils.h"

void ToggleBetweenCharacterAndSpace (int position, char toggleWith, std::string &str) {
    str[position] = (str[position] == toggleWith) ? ' ' : toggleWith;
}