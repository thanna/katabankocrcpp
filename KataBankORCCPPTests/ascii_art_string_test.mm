//
//  ascii_art_string_test.mm
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/15/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import <iostream>
#import "ascii_art_string.h"

@interface ascii_art_string_test : SenTestCase

@end

@implementation ascii_art_string_test

- (void) testParseStringArray {
    std::string oneThroughNine[3] = {
        std::string(" _     _  _     _  _  _  _  _ "),
        std::string("| |  | _| _||_||_ |_   ||_||_|"),
        std::string("|_|  ||_  _|  | _||_|  ||_| _|")};
    
    std::unique_ptr<AsciiArtString> parsedString(AsciiArtString::ParseStringArray(oneThroughNine));
    STAssertTrue(strcmp("0123456789", parsedString->to_string().c_str()) == 0,
         [NSString stringWithFormat:@"Expected %s, got %s", "0123456789", parsedString->to_string().c_str()]
    );
    STAssertFalse(parsedString->HasUnrecognizedCharacter(), @"All characers should have been recognized");
}

- (void) testUnrecognizedCharacter {
    std::string oneThroughNine[3] = {
        std::string(" _     _  _     _  _  _  _  _ "),
        std::string("| | _| _| _||_||_ |_   ||_||_|"),
        std::string("|_|  ||_  _|  | _||_|  ||_| _|")};
    
    std::unique_ptr<AsciiArtString> parsedString(AsciiArtString::ParseStringArray(oneThroughNine));
    STAssertTrue(strcmp("0?23456789", parsedString->to_string().c_str()) == 0,
                 [NSString stringWithFormat:@"Expected %s, got %s", "0?23456789", parsedString->to_string().c_str()]
                 );
    STAssertTrue(parsedString->HasUnrecognizedCharacter(), @"First character should not have been recognized");
}

@end
