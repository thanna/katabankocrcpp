//
//  ascii_art_character_test.m
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/14/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "ascii_art_character.h"

@interface ascii_art_character_test : SenTestCase

@end


@implementation ascii_art_character_test

- (void) testParseParseCharArray {
    for (int i = 0; i < numberOfCharacters; i++) {
        char *characterData = &testInputs[i][0][0];
        char expectedCharacter = testResults[i];

        std::unique_ptr<AsciiArtCharacter> character(AsciiArtCharacter::ParseCharacterArray(characterData));

        STAssertTrue(character->IsRecognized(), @"Character was not recognized");
        STAssertEquals(expectedCharacter, character->character(), nil);
    }
}

- (void) testUnrecognizedCharacter {
    char badData[3][3] = {' ',' ',' ',' ',' ',' ',' ',' ',' '};
    std::unique_ptr<AsciiArtCharacter> character(AsciiArtCharacter::ParseCharacterArray(&badData[0][0]));
    
    STAssertFalse(character->IsRecognized(), @"Invalid character was recognized as something");
    STAssertEquals('?', character->character(), nil);
}

- (void) testPossibleSubstitution {
    std::unique_ptr<AsciiArtCharacter> character(AsciiArtCharacter::ParseCharacterArray(&testInputs[8][0][0]));
    std::vector<char> substitutions = character->PossibleSubstitutions();
    STAssertTrue(substitutions.size() == 3, @"Expected 3 substituions for the number 8, found %i", substitutions.size());
    
    if (std::find(substitutions.begin(), substitutions.end(), '0') == substitutions.end()) {
        STFail(@"Expected 0 to be a substitution for 8");
    }

    if (std::find(substitutions.begin(), substitutions.end(), '6') == substitutions.end()) {
        STFail(@"Expected 0 to be a substitution for 8");
    }

    if (std::find(substitutions.begin(), substitutions.end(), '9') == substitutions.end()) {
        STFail(@"Expected 0 to be a substitution for 8");
    }
}

static int numberOfCharacters = 10;
static char testResults[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
static char testInputs[10][3][3] = {
    ' ','_',' ',
    '|',' ','|',
    '|','_','|',
    
    ' ',' ',' ',
    ' ',' ','|',
    ' ',' ','|',
    
    ' ','_',' ',
    ' ','_','|',
    '|','_',' ',
    
    ' ','_',' ',
    ' ','_','|',
    ' ','_','|',
    
    ' ',' ',' ',
    '|','_','|',
    ' ',' ','|',
    
    ' ','_',' ',
    '|','_',' ',
    ' ','_','|',
    
    ' ','_',' ',
    '|','_',' ',
    '|','_','|',
    
    ' ','_',' ',
    ' ',' ','|',
    ' ',' ','|',
    
    ' ','_',' ',
    '|','_','|',
    '|','_','|',
    
    ' ','_',' ',
    '|','_','|',
    ' ','_','|'
    
};
@end
