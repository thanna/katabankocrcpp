//
//  string_utils_test.m
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/14/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "string_utils.h"

@interface string_utils_test : SenTestCase

@end

@implementation string_utils_test

- (void)testToggleBetweenCharAndSpace {
    std::string originalValue("ToggleSomething");
    std::string expectedResult("Toggl Something");
    std::string result("ToggleSomething");
    
    ToggleBetweenCharacterAndSpace(5, 'e', result);
    
    STAssertTrue(expectedResult == result,
        [NSString stringWithFormat:@"Expected %s, got %s", expectedResult.c_str(), result.c_str()]
    );

    ToggleBetweenCharacterAndSpace(5, 'e', result);
    
    STAssertTrue(originalValue == result,
        [NSString stringWithFormat:@"Expected %s, got %s", originalValue.c_str(), result.c_str()]
    );
}

@end
