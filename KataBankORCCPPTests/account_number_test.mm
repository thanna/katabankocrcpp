//
//  account_number_test.mm
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/17/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "account_number.h"

@interface account_number_test : SenTestCase

@end

@implementation account_number_test

- (void) testValidation {
    AccountNumber zeros("000000000");
    AccountNumber validNumber("457508000");
    AccountNumber invalidNumber("664371495");
    
    STAssertTrue(zeros.IsValid(), @"All zeros should produce a valid checksum");
    STAssertTrue(validNumber.IsValid(), [NSString stringWithFormat:@"%s is valid", validNumber.to_string().c_str()]);
    STAssertFalse(invalidNumber.IsValid(), [NSString stringWithFormat:@"%s is invalid", invalidNumber.to_string().c_str()]);
}

@end
