//
//  main_test.m
//  KataBankOCRCPP
//
//  Created by Travis Hanna on 3/17/13.
//  Copyright (c) 2013 Travis Hanna. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import <sstream>
#import "ocr_application.h"

@interface ocr_application_test : SenTestCase

@end

@implementation ocr_application_test

static OCRApplication *app;

- (void) setUp {
    NSString *filePath = [[NSBundle bundleForClass:[self class] ] pathForResource:@"test-input" ofType:@"txt"];
    app = new OCRApplication([filePath cStringUsingEncoding:NSUTF8StringEncoding]);
}

- (void) tearDown {
    delete app;
}

- (void) testRun {
    std::streambuf *old = std::cout.rdbuf();
    std::stringstream ss;
    
    std::cout.rdbuf (ss.rdbuf());
    int status = app->run();
    std::cout.rdbuf (old);

    STAssertEquals(0, status, @"Execution returned a non-zero (error) status code");

    NSError* error;

    NSString *filePath = [[NSBundle bundleForClass:[self class] ] pathForResource:@"expected-output" ofType:@"txt"];
    NSString *expectedOutput = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    NSString *actualOutput = [NSString stringWithCString:ss.str().c_str() encoding:NSUTF8StringEncoding];
    
    STAssertEqualObjects(expectedOutput, actualOutput, nil);
}
@end
