Kata Bank OCR (C++)
===================
This repository holds a solution for [KataBankOCR](http://codingdojo.org/cgi-bin/wiki.pl?KataBankOCR) written by Travis Hanna.  This solution is a written in C++ and based heavily on [my Java solution](https://bitbucket.org/thanna/katabankocrjava) (though it is not a line-by-line port).

Building
--------
I'm currently building this application with XCode 4.6.1 on MacOS 10.8.2. Unit tests were written in Objective-C using OCUnit.  The XCode project is configured to build the application, run unit tests, and produce code coverage files (Which can be read using a tool such as Cover Story).

Notes
-----
I selected XCode and OCUnit due to familiarity and availability.  Were I to do this problem over again, I would evaluate other options.  While XCode does a fair job of syntax highlighting and command completion for C++, it does not support any meaningful refactoring.  And while I originally thought the more dynamic nature of Objective-C would make it handy for writing compact and readable unit tests, it ended up being a hinderance in the end due to the constant friction between Objective C and C++ types.

When it comes to programming languages, C++ was my first love.  It was my introduction to Object Oriented Programming.  It was also my last line of defense back in the days of client server programming and RAD tools such as PowerBuilder and Delphi.  When one of these tools didn't provide the flexibility to accomplish a task, I always knew that in a pinch, I could write a DLL with Visual C++ and call it from PowerScript or Object Pascal.

That being said, holy crap am I rusty.  It has been over 10 years since I've written a line of C++ and I know that it shows.  Passing objects by value and having objects live on the stack instead of the heap was making my head spin at times.  I had also forgotten how primative C++ arrays are in comparison to more recent languages.  Over time, almost all of my arrays were replaced by vectors.

Having completed this exercise, I'm confident that given 2-3 weeks working in a relatively clean C++ codebase, I would be back in good form.